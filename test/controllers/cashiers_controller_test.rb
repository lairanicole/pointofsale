require 'test_helper'

class CashiersControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get cashiers_index_url
    assert_response :success
  end

  test "should get new" do
    get cashiers_new_url
    assert_response :success
  end

  test "should get edit" do
    get cashiers_edit_url
    assert_response :success
  end

  test "should get show" do
    get cashiers_show_url
    assert_response :success
  end

end
