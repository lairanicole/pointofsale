class AddCommissionToCashiers < ActiveRecord::Migration[5.1]
  def change
    add_column :cashiers, :commission, :decimal
    # change_table :cashiers do |t|
    #   t.decimal :commission, :decimal, :null => false
    #   t.timestamps
    # end
  end
end
