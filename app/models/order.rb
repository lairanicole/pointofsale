class Order < ApplicationRecord
    has_many :products, through: :order_lines
    has_many :order_lines
    belongs_to :cashier
    
    accepts_nested_attributes_for :order_lines
    accepts_nested_attributes_for :products
end
   
