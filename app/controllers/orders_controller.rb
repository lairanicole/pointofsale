class OrdersController < ApplicationController
  before_action :set_order, only: [:edit, :show, :update, :destroy]
  
  def order_summary
    @order = current_cashier.orders.new(order_params)
    i=0 
    @order.order_lines.each do |each_order|
      i += each_order.product.price * each_order.quantity
    end
    @order.total_price = i
    # @cashier.commission = 0.04 * current_cashier.orders.total_price
  end
  
  def daily
    @orders = Order.all
  end
  
  def index
    if current_cashier.status == true
      @orders = Order.all
    else
      sign_out current_cashier
      redirect_to publics_index_path
    end
  end

  def new
    @order = current_cashier.orders.new
  end
  
  def create
    @order = current_cashier.orders.new(order_params)
    
    if @order.save
      redirect_to @order, notice: "Order successfully added."
    else
      render :new
    end
  end
  
  def edit
  end

  def show
  end
  
  def update
    if @order.update(order_params)
      redirect_to orders_path, notice: "Order successfully updated."
    else
      render :edit
    end
  end
  
  def destroy
    if @order.status == true
      @order.status = false
    else
      @order.status = true
    end
    
    if @order.save
      redirect_to orders_path, notice: "Order successfully updated."
    end
  end
  
  private
    def set_order
      @order = Order.find_by(id: params[:id])
      redirect_to orders_path, notice: "Order not found." if @order.nil?
    end
    
    def order_params
      params.require(:order).permit!
    end
end
