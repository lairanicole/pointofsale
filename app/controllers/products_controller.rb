class ProductsController < ApplicationController
  before_action :set_product, only: [:edit, :show, :update, :destroy]
  
  def index
    @products = Product.all
  end

  def new
    @product = Product.new
  end
  
  def create
    @product = Product.new(product_params)
    @product.status = true
    
    if @product.save
      redirect_to products_path, notice: "Product successfully added."
    else
      render :new
    end
  end
  
  def edit
  end

  def show
    @product = Product.find_by(id: params[:id])
    
    if !@product.present?
      redirect_to products_path
    end
  end
  
  def destroy
    if @product.status == true
      @product.status = false
    else
      @product.status = true
    end
    
    if @product.save
      redirect_to products_path, notice: "Product successfully updated."
    end
  end
  
  def update
    if @product.update(product_params)
      redirect_to products_path, notice: "Product successfully updated."
    else
      render :edit
    end
  end
  
  private
    def set_product
      @product = Product.find_by(id: params[:id])
      redirect_to products_path, notice: "Product not found." if @product.nil?
    end
    
    def product_params
      params.require(:product).permit!
    end
end
