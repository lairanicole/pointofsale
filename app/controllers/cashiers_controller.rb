class CashiersController < ApplicationController
  before_action :set_cashier, only: [:edit, :show, :update, :destroy]
  
  def index
    @cashiers = Cashier.all
  end

  def new
    @cashier = current_admin.cashiers.new
  end
  
  def create
    @cashier = current_admin.cashiers.new(cashier_params)
    @cashier.status = true
    @cashier.commission = 0

    
    if @cashier.save
      redirect_to cashiers_path, notice: "Cashier successfully added."
    else
      render :new
    end
  end
  
  def edit
  end

  def show
    @cashier = Cashier.find_by(id: params[:id])
    
    @orders = Order.where(:cashier => @cashier)
    
    if !@cashier.present?
      redirect_to cashiers_path
    end
  end
  
  def destroy
    if @cashier.status == true
      @cashier.status = false
    else
      @cashier.status = true
    end
    
    if @cashier.save
      redirect_to cashiers_path, notice: "Cashier successfully updated."
    end
  end
  
  def update
    if @cashier.update(cashier_params)
      redirect_to cashiers_path, notice: "Cashier successfully updated."
    else
      render :edit
    end
  end
  
  private
    def set_cashier
      @cashier = Cashier.find_by(id: params[:id])
      redirect_to cashiers_path, notice: "Cashier not found." if @cashier.nil?
    end
    
    def cashier_params
      params.require(:cashier).permit!
    end
end
