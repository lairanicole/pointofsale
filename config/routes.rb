Rails.application.routes.draw do
  get 'reports/index'
  #get 'reports/new'
  #get 'reports/edit'
  #get 'reports/show'
  get 'orders/index'
  get 'orders/new'
  get 'order_lines/new'
  get 'order_lines/index'
  #get 'orders/edit'
  #get 'orders/show'
  get 'publics/index'
  #post 'orders/order_summary'
  

  devise_scope :admin do
    authenticated :admin do
      resources :cashiers
      resources :products
      get 'orders/daily', to: 'orders#daily', as: :daily
      resources :reports
      root 'products#index', as: :authenticated_admin_root
    end
  end
  
  devise_scope :cashier do
    authenticated :cashier do
      post 'order_summary', to: 'orders#order_summary', as: :order_summary
      resources :orders
      resources :products
      resources :cashiers, :only => [:show]
      root 'orders#index', as: :authenticated_cashier_root
    end
  end
  
  devise_for :admins
  devise_for :cashiers
  root 'publics#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
